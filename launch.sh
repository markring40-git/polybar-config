#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar
# If all your bars have ipc enabled, you can also use 
# polybar-msg cmd quit

# Launch bar1 and bar2
echo "---" | tee -a /tmp/polybar1.log /tmp/polybar2.log /tmp/polybar3.log /tmp/polybar4.log
polybar mybar0 >>/tmp/polybar1.log 2>&1 & disown
polybar mybar0b >>/tmp/polybar2.log 2>&1 & disown
polybar mybar1 >>/tmp/polybar3.log 2>&1 & disown
polybar mybar1b >>/tmp/polybar4.log 2>&1 & disown

echo "Bars launched..."
